import { AngularFireDatabase } from 'angularfire2/database'
import { Injectable } from '@angular/core'

@Injectable()
export class DocumentsService {
  constructor(public db: AngularFireDatabase) {

  }

  public getDocuments() {
    return this.db.list('/documents/')
  }

  public getDocument(id) {
    return this.db.object(`/documents/${id}`)
  }

  public createDocument(doc) {
    return this.db.database.ref(`/documents/${doc.id}`).set(doc)
  }

  public updateDocument(doc) {
    return this.db.database.ref(`/documents/${doc.id}`).set(doc)
  }

  public deleteDocument(doc) {
    return this.db.database.ref(`/documents/${doc.id}`).remove()
  }
}