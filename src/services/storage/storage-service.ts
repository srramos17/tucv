import { HttpClient } from '@angular/common/http'
import { AngularFireStorage } from 'angularfire2/storage'
import { Injectable } from '@angular/core'
import { finalize } from 'rxjs/operators'

import { DocumentsService } from '../db/document-service'

@Injectable()
export class StorageService {
  constructor(
    private fireBaseStorage: AngularFireStorage,
    private documentsService: DocumentsService,
    public http: HttpClient
  ) {}
  private getShortUrl(url) {
    const api = `https://is.gd/create.php?format=simple&url=${encodeURIComponent(
      url
    )}`
    return this.http.get(api)
  }
  // file: data_url
  public UploadFile(file, { displayName, email, uid }) {
    const fileName = displayName.split(' ').join('-')
    const path = `cvs/${uid}/${fileName}CV.pdf`
    const fileRef = this.fireBaseStorage.ref(path)
    const task = fileRef.putString(file, 'data_url')

    task
      .snapshotChanges()
      .pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe(
            url => {
              this.getShortUrl(url).subscribe(
                res => {},
                err => {
                  if (err.error.text.includes('https://is.gd')) {

                    const doc = {
                      id: uid,
                      downloadURL: url,
                      shortURL: err.error.text,
                      user: email,
                      name: displayName,
                      plan: 'free',
                      planId: 0,
                      date: Date.now()
                    }

                    this.documentsService.createDocument(doc)
                  }
                }
              )
            }
          )
        })
      )
      .subscribe()

    return task.percentageChanges()
  }

  public getFileURL({ uid }) {
    return this.documentsService.getDocument(uid)
  }
}
