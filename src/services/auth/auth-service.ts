import { Injectable } from '@angular/core'
import { AngularFireDatabase } from 'angularfire2/database'
import { AngularFireAuth } from 'angularfire2/auth'

@Injectable()
export class AuthService {
  constructor(public sb: AngularFireDatabase, public fireAuth: AngularFireAuth) {

  }

  SignIn (way) {
    //todo
  }

  logout () {
    return this.fireAuth.auth.signOut()
  }
}