import { Component, ViewChild } from '@angular/core'
import { Platform, Nav } from 'ionic-angular'
import { StatusBar } from '@ionic-native/status-bar'
import { SplashScreen } from '@ionic-native/splash-screen'
import { TranslateService } from '@ngx-translate/core'
import { Network } from '@ionic-native/network'

import { FirstRunPage } from '../pages'

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = FirstRunPage
  defaultLanguage: string = 'en'

  @ViewChild(Nav)
  nav: Nav

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private translate: TranslateService,
    private network: Network
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault()
      splashScreen.hide()
    })

    // platform.pause.subscribe(() => alert('App was paused'))
    // platform.resume.subscribe(() => alert('App resumed'))

    this.initTranslate()
    this.subscribeNetwork()
  }

  initTranslate() {
    this.translate.setDefaultLang('es')

    const browserLang = this.translate.getBrowserLang()

    if (browserLang) {
      this.translate.use(browserLang)
      return
    }

    this.translate.use(this.defaultLanguage)
  }

  subscribeNetwork () {
    this.network.onConnect().subscribe(() => {
      // FUnction to do something when network is avaiable.

      // We just got a connection but we need to wait briefly
      // before we determine the connection type. Might need to wait.
      // prior to doing any api requests as well.
      setTimeout(() => {
        // Now we can do anything network related such API calls or something.
      })
    })

    this.network.onDisconnect().subscribe(() => {
      // FUnction to do something when network is not avaiable.
    })
  }

}
