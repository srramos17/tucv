import { HttpClient, HttpClientModule } from '@angular/common/http'
import { NgModule, ErrorHandler } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular'
import { StatusBar } from '@ionic-native/status-bar'
import { SplashScreen } from '@ionic-native/splash-screen'

import { TranslateModule, TranslateLoader } from '@ngx-translate/core'
import { TranslateHttpLoader } from '@ngx-translate/http-loader'
import { NgxQRCodeModule } from 'ngx-qrcode2'
import { File } from '@ionic-native/file'
import { FilePath } from '@ionic-native/file-path'
import { FileChooser } from '@ionic-native/file-chooser' // for Android
import { DocumentPicker } from '@ionic-native/document-picker'// for IOs
import { AngularFireModule } from 'angularfire2'
import { AngularFirestoreModule } from 'angularfire2/firestore'
import { AngularFireDatabaseModule } from 'angularfire2/database'
import { AngularFireStorageModule } from 'angularfire2/storage'
import { AngularFireAuthModule } from 'angularfire2/auth'
import { GooglePlus } from '@ionic-native/google-plus'
import { SocialSharing } from '@ionic-native/social-sharing'
import { Network } from '@ionic-native/network'

import { MyApp } from './app.component'
import { AccountPage } from '../pages/account/account'
import { ContactPage } from '../pages/contact/contact'
import { HomePage } from '../pages/home/home'
import { TabsPage } from '../pages/tabs/tabs'
import { LandPage } from '../pages/land/land'
import { LoginPage } from '../pages/login/login'
import { ModalFaqPage } from '../pages/modal-faq/modal-faq'
import { environment } from '../firebase.config'

import { StorageService } from '../services/storage/storage-service'
import { DocumentsService } from '../services/db/document-service'
import { AuthService } from '../services/auth/auth-service'

// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json')
}

@NgModule({
  declarations: [
    MyApp,
    AccountPage,
    ContactPage,
    HomePage,
    TabsPage,
    LandPage,
    LoginPage,
    ModalFaqPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    IonicModule.forRoot(MyApp),
    NgxQRCodeModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireStorageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AccountPage,
    ContactPage,
    HomePage,
    TabsPage,
    LandPage,
    LoginPage,
    ModalFaqPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    StatusBar,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    File,
    FilePath,
    FileChooser,
    DocumentPicker,
    StorageService,
    DocumentsService,
    AuthService,
    GooglePlus,
    SocialSharing,
    Network
  ]
})
export class AppModule {}
