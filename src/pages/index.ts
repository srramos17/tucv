import { LandPage } from './land/land'
import { TabsPage } from './tabs/tabs'

// The page the user lands on after opening the app and without a session
export const FirstRunPage = LandPage

// The main page the user will see as they use the app over a long period of time.
export const MainPage = TabsPage