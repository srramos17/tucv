import { Component } from '@angular/core'
import { IonicPage, NavController } from 'ionic-angular'
import { TranslateService } from '@ngx-translate/core'

import { AccountPage } from '../account/account'
import { ContactPage } from '../contact/contact'
import { HomePage } from '../home/home'

@IonicPage()
@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  account: string = 'Account'

  tabHome = HomePage
  tabAccount = AccountPage
  tabInfo = ContactPage

  constructor(public navCtrl: NavController, translate: TranslateService) {
    translate.get('TAB-1-TITLE').subscribe(res => this.account = res)
  }

  ionViewDidLoad() {

  }
}
