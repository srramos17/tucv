import { Component } from '@angular/core'
import {
  IonicPage,
  NavController,
  NavParams,
  Platform,
  MenuController
} from 'ionic-angular'
import { TranslateService } from '@ngx-translate/core'
import { Slide } from '../../interfaces/slides'
import { Words } from '../../enums/words'
import { LoginPage } from '../login/login'
import { AngularFireAuth } from 'angularfire2/auth'
import { TabsPage } from '../tabs/tabs'

@IonicPage()
@Component({
  selector: 'page-land',
  templateUrl: 'land.html'
})
export class LandPage {
  slides: Slide[] = []
  showSkip: boolean = true
  showSlides = false
  dir: string = 'ltr'

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private translate: TranslateService,
    public platform: Platform,
    public menu: MenuController,
    private afAuth: AngularFireAuth
  ) {
    //Set language direction from device
    this.dir = platform.dir()
  }

  ionViewDidLoad () {
    this.afAuth.authState.subscribe(user => {
      if (user && user.uid) {
        this.navCtrl.setRoot(TabsPage)
      } else {
        this.translate
          .get([
            Words.landingSlide1Title,
            Words.landingSlide1Description,
            Words.landingSlide2Description,
            Words.landingSlide3Description,
            Words.landingSlide4Description
          ])
          .subscribe(values => {
            for (let i = 1; i < 4; i++) {
              this.slides.push({
                title: values[`LANDING-SLIDE-${i}-TITLE`],
                description: values[`LANDING-SLIDE-${i}-DESCRIPTION`],
                image: `assets/img/slide-${i}.svg`
              })
            }

            this.showSlides = true
          })
      }
    })
  }

  ionViewDidEnter() {
    this.menu.enable(false)
  }

  ionViewWillLeave() {
    this.menu.enable(true)
  }

  startApp() {
    this.navCtrl.setRoot(
      LoginPage,
      {},
      {
        animate: true,
        direction: 'forward'
      }
    )
  }

  onSlideChangeStart(slider) {
    this.showSkip = !slider.isEnd()
  }
}
