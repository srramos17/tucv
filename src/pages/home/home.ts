import { Component } from '@angular/core'
import {
  NavController,
  Platform,
  LoadingController,
  FabContainer
} from 'ionic-angular'
import { File } from '@ionic-native/file'
import { FilePath } from '@ionic-native/file-path'
import { FileChooser } from '@ionic-native/file-chooser' // for Android
import { DocumentPicker } from '@ionic-native/document-picker' // for IOs
import { AngularFireAuth } from 'angularfire2/auth'
import { AngularFireDatabase } from 'angularfire2/database'
import { SocialSharing } from '@ionic-native/social-sharing'
import { TranslateService } from '@ngx-translate/core'
import domtoimage from 'dom-to-image'

import { StorageService } from '../../services/storage/storage-service'
import { DocumentsService } from '../../services/db/document-service'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  isIos: boolean = false
  uploadPercent: number = 0
  fileURL: string
  user: firebase.User
  userHasUploadedCV = false
  uploadedFile: any = {}
  maximuFileSize: number = 10 // 10 MB
  showPage = false
  qrImage: any

  constructor(
    platform: Platform,
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    private fileHelper: File,
    private filePath: FilePath,
    private filePicker: FileChooser,
    private iosFilePicker: DocumentPicker,
    public afAuth: AngularFireAuth,
    public afDb: AngularFireDatabase,
    private storageService: StorageService,
    private socialSharing: SocialSharing,
    private translate: TranslateService,
    private documentsService: DocumentsService
  ) {
    this.isIos = platform.is('ios')
  }

  ionViewDidLoad () {
    this.afAuth.authState.subscribe(user => {
      this.user = user
      this.checkIfUserHasUploadedFile()
    }, err => this.showPage = true)
  }

  bytesToSize(bytes) {
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB']
    if (bytes === 0) return '0 Byte'

    const i = parseInt(Math.floor(Math.log(parseInt(bytes)) / Math.log(1024)).toString())
    return Math.round(bytes / Math.pow(1024, i)) + ' ' + sizes[i]
 }

  allowedFormatFile (file) {
    if (file) {
      const rgx = new RegExp(/^(data:application\/pdf;)/)
      const type = file.split(',').shift()

      return rgx.test(type)
    }

    return false
  }

  allowedSizeFile (file) {
    const fileLen = file.split(',').pop().length
    const bytes = (fileLen * (3/4)) - 2
    const size = this.bytesToSize(bytes).split(' ')

    if(parseInt(size[0]) > this.maximuFileSize && size[1] === 'MB') {
      return false
    }

    return true
  }

  showUploadError () {
    this.translate.get('UPLOAD-ERROR')
        .subscribe(res => {
          alert(res)
        })
  }

  async uploadCv(fab?: FabContainer) {
    this.uploadPercent = 0
    if (fab) {
      this.closeFab(fab)
    }

    if (this.isIos) {
      try {
        this.iosFilePicker
          .getFile('pdf')
          .then(uri => console.log(uri))
          .catch(e => console.log(e))
        return
      } catch (e) {
        alert(`ERROR IOS: ${JSON.stringify(e)}`)
      }
    }

    try {
      const pdfFile = await this.filePicker
        .open()
        .then(uri => this.filePath.resolveNativePath(uri))
        .then(url => this.fileHelper.resolveLocalFilesystemUrl(url))
        .then(res => ({
          path: res.nativeURL.replace(encodeURI(res.name), ''),
          file: res.name
        }))
        .then(res => this.fileHelper.readAsDataURL(res.path, res.file)) // pdfFile base64 pdf
        .then(file => {
          if (this.allowedFormatFile(file)) {
            if (this.allowedSizeFile(file)) {
              return file
            }

            return this.translate.get('EXCEED-FILE-SIZE')
              .subscribe(res => {
                alert(res)
                throw Error(res)
              })
          }

          this.translate.get('FILE-INVALID-FORMAT')
            .subscribe(res => {
              alert(res)
              throw Error(res)
            })
        })
        .catch((e) => {
          this.showUploadError()
          return ''
        })

      this.storageService.UploadFile(pdfFile, this.user).subscribe(progress => {
        this.uploadPercent = progress

        if (progress === 100) {
          this.checkIfUserHasUploadedFile()
        }
      })
    } catch (e) {
      this.showUploadError()
    }
  }

  dropCV (fab?: FabContainer) {
    if (fab) {
      this.closeFab(fab)
    }

    this.documentsService.deleteDocument({ id: this.user.uid })
  }

  reUploadCv (fab: FabContainer) {
    this.uploadCv(fab)
  }

  getQRData() {
    return domtoimage.toJpeg(document.getElementById('qrcv'), { quality: 0.95 })
  }

  checkIfUserHasUploadedFile() {
    this.storageService
      .getFileURL(this.user)
      .valueChanges()
      .subscribe(file => {
        this.userHasUploadedCV = false

        if (file) {
          this.uploadedFile = file
          delete this.uploadedFile.user
          delete this.uploadedFile.planId
          delete this.uploadedFile.plan
          delete this.uploadedFile.name
          this.userHasUploadedCV = true

          setTimeout(() => {
            this.getQRData().then(dt => {
              this.uploadPercent = 0
              this.qrImage = dt
            })
          }, 1000)
        }

        this.showPage = true
      }, err => this.showPage = true)
  }

  downloadQR() {
    this.getQRData().then(dt => {
      this.socialSharing.saveToPhotoAlbum(dt)
    })
  }

  closeFab(fab: FabContainer) {
    fab.close()
  }

  share(fab: FabContainer) {
    this.closeFab(fab)

    const fileName = this.user.displayName.split(' ').join('-') + 'CV'

    this.socialSharing
        .share(null, fileName, this.qrImage, this.uploadedFile.shortURL)
        .catch(err => alert(`ERR: ${err}`))
  }
}
