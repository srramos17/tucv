import { Component } from '@angular/core'
import { App } from 'ionic-angular'
import { NavController } from 'ionic-angular'
import { AngularFireAuth } from 'angularfire2/auth'
import { AlertController } from 'ionic-angular'

import { LandPage } from '../land/land'

@Component({
  selector: 'page-account',
  templateUrl: 'account.html'
})
export class AccountPage {
  pdfUrl: string = decodeURI('')
  user: firebase.User
  constructor(
    private app: App,
    public navCtrl: NavController,
    private afAuth: AngularFireAuth,
    private alertCtrl: AlertController) {
    this.afAuth.authState.subscribe(user => {
      this.user = user
    })
  }

  signOut() {
    let alert = this.alertCtrl.create({
      title: 'really?',
      message: 'Are you sure you want to logout?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {}
        },
        {
          text: 'Confirm',
          handler: () => {
            this.afAuth.auth.signOut()
            .then(() => this.app.getRootNavs()[0].setRoot(LandPage))
            .catch(err => console.log(err))
          }
        }
      ]
    })
    alert.present()
  }

}
