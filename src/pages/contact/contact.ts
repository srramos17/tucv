import { Component } from '@angular/core'
import { NavController, ModalController } from 'ionic-angular'
import { ModalFaqPage } from '../modal-faq/modal-faq'
import { TranslateService } from '@ngx-translate/core'
import { Words } from '../../enums/words'

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  faqs = []
  amountFaqs = 4

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    translate: TranslateService)
    
    {
      translate
          .get([
            Words.question1,
            Words.question1Answer,
            Words.question1Icon,          
            Words.question2,
            Words.question2Answer,
            Words.question2Icon,          
            Words.question3,
            Words.question3Answer,
            Words.question3Icon,          
            Words.question4,
            Words.question4Answer,
            Words.question4Icon                    
            
                      
          ])
          .subscribe(values => {
            for (let i = 1; i < this.amountFaqs+1; i++) {
              this.faqs.push({
                question: values[`QUESTION-${i}`],
                answer: values[`QUESTION-${i}-ANSWER`],
                icon: values[`QUESTION-${i}-ICON`]
              })
            }        
          })
  }

  openModal (faq) {
    const modal = this.modalCtrl.create(ModalFaqPage, faq)
    modal.present()
  }

}
