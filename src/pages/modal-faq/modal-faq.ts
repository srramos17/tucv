import { Component } from '@angular/core'
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular'
@IonicPage()
@Component({
  selector: 'page-modal-faq',
  templateUrl: 'modal-faq.html',
})
export class ModalFaqPage {
  faq: any
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.faq = this.navParams.get('question')
  }

  dismiss() {
    this.viewCtrl.dismiss()
  }

}
