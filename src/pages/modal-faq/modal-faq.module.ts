import { NgModule } from '@angular/core'
import { IonicPageModule } from 'ionic-angular'
import { ModalFaqPage } from './modal-faq'

@NgModule({
  declarations: [
    ModalFaqPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalFaqPage),
  ],
})
export class ModalFaqPageModule {}
