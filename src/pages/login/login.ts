import { Component } from '@angular/core'
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular'
import * as firebase from 'firebase/app'
import { AngularFireAuth } from 'angularfire2/auth'
import { GooglePlus } from '@ionic-native/google-plus'

import { TabsPage } from '../tabs/tabs'
import { TranslateService } from '@ngx-translate/core'

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private afAuth: AngularFireAuth,
    private gplus: GooglePlus,
    private platform: Platform,
    private translate: TranslateService
  ) {}

  showLoginError() {
    this.translate.get('TRY-AGAIN').subscribe(res => {
      alert(res)
    })
  }

  async nativeGoogleLogin(): Promise<void> {
    try {
      const gplusUser = await this.gplus.login({
        webClientId:
          '325585249146-pua4d6nqh9ke9q5g820op98i5v4d4e5e.apps.googleusercontent.com',
        offline: true,
        scopes: 'profile email'
      })

      this.afAuth.auth
        .signInWithCredential(
          firebase.auth.GoogleAuthProvider.credential(
            gplusUser.idToken,
            gplusUser.accessToken
          )
        )
        .then(res => {
          if (res && res.uid) {
            this.navCtrl.setRoot(TabsPage)
          }
        })
        .catch((err) => {
          alert(`FB-AUTH:ERROR::${JSON.stringify(err)}`)
          this.showLoginError()
        })
    } catch (err) {
      alert('GLOBAL-ERROR::'+JSON.stringify(err))
      this.showLoginError()
    }
  }

  async webGoogleLogin(): Promise<firebase.auth.UserCredential> {
    try {
      const provider = new firebase.auth.GoogleAuthProvider()
      return await this.afAuth.auth.signInWithPopup(provider)
    } catch (err) {
      this.showLoginError()
    }
  }

  googleLogin() {
    if (this.platform.is('android')) {
      this.nativeGoogleLogin()
    } else {
      this.webGoogleLogin()
    }
  }

  signOut() {
    this.afAuth.auth.signOut()
  }
}
