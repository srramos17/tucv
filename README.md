
# TuCV

The best way to get and share your CV by QR.

## Scripts
- `npm run lint`  Show all warnings
- `npm run lint-fix` Fix all warnings
- `ionic serve` Run the app


## Instalation Steps

- Make a pull to dev branch `git pull origin dev`

- run `ionic serve` to start the project.

- Start to code!



### Rules for code

- No semicolons `;`

- No double quotes `"eyy"` Single quotes instead `'eyy'`



### Development process

- Before start to code, create a new branch from dev branch and begin write in this new branch. `git checkout -b branch-name`

- Once you finish your development, commit your changes. `git commit -m "--your comment here--"`

- Switch to dev branch and make a pull.
`1. git checkout dev`
`2. git pull origin dev`

- Switch back to your branch and make a merge (resolve conflicts if apply)
`1. git checkout branch-name`
`2. git merge dev`

- Make push to your branch
`git push origin branch-name`

- Finally make a PR (pull request) from your branch againts dev branch.

- Wait for approval.

- Repeat every single previous step for push your changes on prod.